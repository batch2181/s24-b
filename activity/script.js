
getCube = 2 ** 3;
console.log(`The cube of 2 is ${getCube}`);

const address = [258, "Washington Ave NW", "California", 90011];
const[number, avenue, state, zipCode] = address;
console.log(`I live at ${number} ${avenue}, ${state} ${zipCode}`);

const animal = {
	name: "Lolong",
	weight: "1075 kgs",
	measurement: "20 ft 3 in"
}

const {name, weight, measurement} = animal;
console.log(`${name} was a saltwater crocodile. He weighed at ${weight} with a measurement of ${measurement}.`)

const numbers = [1, 2, 3, 4, 5];

numbers.forEach((number) => {
    console.log(number)
})

let displayReduce = numbers.reduce((accumulator, currentValue) => accumulator + currentValue);
console.log(displayReduce);


class Dog {
	constructor(name, age, breed) {
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}

const myPetDog = new Dog();

myPetDog.name = "Frankie";
myPetDog.age = 5;
myPetDog.breed = "Miniature Dachshund"
console.log(myPetDog)


